/* внутренний отступ для body, чтобы контент не залазил под шапку */
$('BODY').css( 'padding-top', $('.header.fixed-top').outerHeight() + parseFloat($('.header.fixed-top').css('margin-top')) + parseFloat($('.header.fixed-top').css('margin-bottom')) );
/* \\ внутренний отступ для body, чтобы контент не залазил под шапку */


/* безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */
$(document).on('click', '[target="_blank"]', function(e) {
    e.preventDefault();

    var otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = $(this).attr('href');
});
/* \\ безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */


// липкая шапка
$(document).on('scroll ready', function() {
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if ( scrollTop > 0 && !$('.header').hasClass('fixed') ) {
        $('.header').addClass('fixed');
    } else if ( scrollTop == 0 ) {
        $('.header.fixed').removeClass('fixed');
    }
});


// адаптивные таблицы для мобилок
if ( $(window).width() < 768 ) {
    $('TABLE').wrap('<div class="table-responsive-md"></div>')
}


$(document).ready(function() {
    // скрываем прелоадер
    $('#preloader').fadeOut(500);

    // запрет ввода любых символов кроме цифр в поле для ввода номера телефона
    $(document).on('input', '[name="phone"]', function() {
        var val = $(this).val(),
            input = $(this);
        (parseInt(val) == 0) ? $(this).val(val.replace(/\D/g, 1)) : $(this).val(val.replace(/\D/g, ''));
    });

    // сворачивание/разворачивание блоков
    $(document).on('click', '[data-toggle]', function(e) {
        e.preventDefault();

        $( $(this).data('toggle') ).toggleClass('d-none');
        $(this).toggleClass('active');
    });

    //инициализация popup gallery
    (function initPopupGallery() {
        var gallery = document.querySelector('[data-popup-gallery]');
        if(!gallery) return;

        $('[data-popup-gallery]').each(function() {
            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Загрузка изображения #%curr%...',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1], // Will preload 0 - before current, and 1 after the current image
                    tCounter: '' // markup of counter
                },
                image: {
                    tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.',
                }
            });
        });
    })();

    // ajax запросы при нажатии на кнопки с [data-ajax]
    $(document).on('click', '[data-ajax]', function(e) {
        e.preventDefault();

        var self = $(this),
            action = self.data('ajax'),
            snippet_params = $.parseJSON( self.data('params').replace(/'/g, '"') ),
            reload = self.data('ajax-reload'),
            form  = ( $('#' + self.attr('form')).length > 0 ) ? $('#' + self.attr('form').get(0)) : // если у элемента с атрибутом data-ajax есть атрибут form получаем связанную с ним форму
                    ( self.parent('form').length > 0 ? self.parent('form').get(0) : // если элемент лежит непосредственно в форме
                    ( self.parentsUntil('form').length > 0 ) ? self.parentsUntil('form').parent().get(0) : null ); // если элемент лежит на любом уровне вложенности в форме

        // собираем данные с формы
        params = new FormData( form );
        params.append( 'action', action );
        for (key in snippet_params)
            params.append( key, decodeURIComponent(snippet_params[key]) );

        // отправляем запрос
        $.ajax({
            contentType: false, // важно - убираем форматирование данных по умолчанию
            processData: false, // важно - убираем преобразование строк по умолчанию
            dataType: 'json', // тип ожидаемых данных в ответе
            type: 'POST',
            data: params,
            success: function() {
                // очищаем статусы полей
                form.find('.input.success, .input.error').removeClass('success error');
            }
        });
    });

    //
    $('.input_custom-file [type="file"]').on('change', function(e) {
        var filename_container = $(this).siblings('[data-custom-file-value]');

        filename_container.text(e.target.files[0].name);

        if ( !$(this).parent('.input_custom-file').hasClass('selected') )
            $(this).parent('.input_custom-file').addClass('selected');
    });

    // POPUPS
    $('[data-popup]').each(function() {
        var popup_type = $(this).data('popup') ? $(this).data('popup') : 'inline';

        if ( $(this).data('popup-header') ) {
            var popup_id = $(this).attr('href').replace('#', ''),
                popup_header_el = $('#' + popup_id + ' .popup__header'),
                new_popup_header = $(this).data('popup-header'),
                default_popup_header = popup_header_el.text();
        }

        if ( popup_type == 'image' )
            var $showCloseBtn = true;

        $(this).magnificPopup({
            type: popup_type,
            showCloseBtn: $showCloseBtn,
            callbacks: {
                beforeOpen: function() {
                    if ( !popup_id && !new_popup_header )
                        return;

                    popup_header_el.text(new_popup_header);
                },

                afterClose: function() {
                    // убираем выделение незаполненных обязательных полей
                    $('.input.error').removeClass('error');

                    if ( !popup_id && !new_popup_header )
                        return;

                    popup_header_el.text(default_popup_header);
                }
            }
        });
    });

    // кнопка закрытия magnific popup
    $(document).on('click', '[data-mfp-close]', function() {
        $.magnificPopup.close();
    });
});


/* действия по успешной отправке формы через AjaxForm */
$(document).on('af_complete', function(event, response) {
    // автоматическое закрытие magnificpopup при успешной отправке формы
    if (response.success)
        $.magnificPopup.close();
});
/* \\ действия по успешной отправке формы через AjaxForm */


/* YANDEX MAPS */
(function maps() {

    var yandexMapsInit = function (c) {

        if (!$('[data-map').length) return false;


        $.getScript('https://api-maps.yandex.ru/2.1?apikey=' + $yandex_maps_api_key + '&lang=ru_RU', function () {

            ymaps.ready(init);

            function init() {
                $('[data-map]').each(function () {
                    var $t = $(this),
                        address = $t.data('map');

                    $t.html('');

                    ymaps.geocode(address, {
                        results: 1
                    }).then(function (res) {
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates();


                        var placemark = new ymaps.Placemark(coords, {
                            balloonContent: firstGeoObject.properties._data.balloonContent
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/assets/images/map_marker.png',
                            iconImageSize: [63, 81],
                            iconImageOffset: [-31, -80],
                        });


                        var myMap = new ymaps.Map($t.attr('id'), {
                            center: coords,
                            zoom: 16,
                            controls: ['zoomControl', 'typeSelector']
                        });
                        myMap.behaviors.disable("scrollZoom");
                        myMap.geoObjects.add(placemark);
                    });
                });
            }
        });
    }

    yandexMapsInit($(this));
})();
/* \\ YANDEX MAPS */
