$(document).ready(function() {
    var mainpage_slider = new Swiper('#mainpage_slider', {
        centeredSlides: true,
        effect: 'fade',
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var reviews_slider = new Swiper('#reviews_slider', {
        slidesPerView: 3,
        spaceBetween: 10,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            767: {
              slidesPerView: 1,
            },
            1199: {
              slidesPerView: 2,
            },
        },
        on: {
            init: function() {
                var slider = this;

                // управление автопереключением при наведении курсора
                slider.el.addEventListener('mouseenter', function() {
                    slider.autoplay.stop();
                });
        
                slider.el.addEventListener('mouseleave', function() {
                    slider.autoplay.start();
                });
            }
        }
    });

    var boarding_house_page_slider_switcher = new Swiper('#boarding_house_page_slider_switcher', {
        spaceBetween: 10,
        slidesPerView: 4,
        slidesPerColumn: 1,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
        },
        breakpoints: {
            992: {
                slidesPerView: 2,
                slidesPerColumn: 2,
            }
        }
    });

    var boarding_house_page_slider = new Swiper('#boarding_house_page_slider', {
        centeredSlides: true,
        effect: 'fade',
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: boarding_house_page_slider_switcher
        }
    })
    
});